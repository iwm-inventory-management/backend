﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_Core.Usecases.Warehouses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.ObjectMothers
{
    internal class LocationMother
    {
        private IWarehouseRepository _warehouseRepository;
        private ILocationRepository _locationRepository;

        public LocationMother(IWarehouseRepository warehouseRepository, ILocationRepository locationRepository)
        {
            _warehouseRepository = warehouseRepository;
            _locationRepository = locationRepository;
        }

        /// <summary>
        /// Instanciate a new <see cref="Location"/> object that is registered in the <see cref="ILocationRepository"/>.
        /// Requires a registered <see cref="Warehouse"/>.
        /// </summary>
        /// <param name="locationId">ID of the new <see cref="Location"/></param>
        /// <param name="warehouse">Registered <see cref="Warehouse"/></param>
        /// <returns>An registered <see cref="Location"/> object with the given ID.</returns>
        public Location GetRegisteredLocation(string locationId, Warehouse warehouse)
        {
            Location location = new Location(locationId, warehouse);
            var createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);

            createLocationUsecase.Execute(location);
            return _locationRepository.GetByNameAndWarehouseId(locationId, warehouse.Id);
        }

        /// <summary>
        /// Instanciate a new <see cref="Location"/> object that is registered in the <see cref="ILocationRepository"/>.
        /// Requires a registered <see cref="Warehouse"/>.
        /// </summary>
        /// <param name="warehouse">Registered <see cref="Warehouse"/></param>
        /// <returns>An registered <see cref="Location"/> object with a random ID.</returns>
        public Location GetRegisteredLocation(Warehouse warehouse)
        {
            return GetRegisteredLocation(RandomStringGenerator.GenerateRandomString(9), warehouse);
        }

        /// <summary>
        /// Instanciate a new <see cref="Location"/> object that is registered in the <see cref="ILocationRepository"/>.
        /// Also register a brand new <see cref="Warehouse"/> with a random ID.
        /// </summary>
        /// <returns>An registered <see cref="Location"/> object with a random ID.</returns>
        public Location GetRegisteredLocationInRegisteredWarehouse()
        {
            var warehouseMother = new WarehouseMother(_warehouseRepository);
            Warehouse generatedWarehouse = warehouseMother.GetRegisteredWarehouse();
            return GetRegisteredLocation(RandomStringGenerator.GenerateRandomString(9), generatedWarehouse);
        }
    }
}
