﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Articles;
using IWM_IM_Core.Usecases.Locations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.ObjectMothers
{
    internal class ArticleMother
    {
        private IArticleRepository _articleRepository;

        public ArticleMother(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        /// <summary>
        /// Instanciate a new <see cref="Article"/> object that is registered in the <see cref="IArticleRepository"/>.
        /// </summary>
        /// <param name="articleId">ID of the new <see cref="Article"/></param>
        /// <returns>An registered <see cref="Article"/> object with the given ID.</returns>
        public Article GetRegisteredArticle(string articleId)
        {
            Article article = new Article(articleId);
            var createArticleUsecase = new CreateArticleUsecase(_articleRepository);

            createArticleUsecase.Execute(article);
            return _articleRepository.GetById(articleId);
        }

        /// <summary>
        /// Instanciate a new <see cref="Article"/> object that is registered in the <see cref="IArticleRepository"/>.
        /// </summary>
        /// <param name="articleId">ID of the new <see cref="Article"/></param>
        /// <returns>An registered <see cref="Article"/> object with a random ID.</returns>
        public Article GetRegisteredArticle()
        {
            return GetRegisteredArticle(RandomStringGenerator.GenerateRandomString(6));
        }
    }
}
