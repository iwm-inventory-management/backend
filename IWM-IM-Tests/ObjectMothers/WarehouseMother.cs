﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Warehouses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.ObjectMothers
{
    internal class WarehouseMother
    {
        internal readonly string MAG_ID = "MAG";

        private IWarehouseRepository _warehouseRepository;

        public WarehouseMother(IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
        }

        /// <summary>
        /// Instanciate a new <see cref="Warehouse"/> object that is registered in the <see cref="IWarehouseRepository"/>.
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <returns>An registered <see cref="Warehouse"/> object with the given ID.</returns>
        public Warehouse GetRegisteredWarehouse(string warehouseId)
        {
            Warehouse warehouse = new Warehouse(warehouseId);
            var createWarehouseUsecase = new CreateWarehouseUsecase(_warehouseRepository);

            createWarehouseUsecase.Execute(warehouse);
            return _warehouseRepository.Get(warehouseId);
        }

        /// <summary>
        /// Instanciate a new <see cref="Warehouse"/> object that is registered in the <see cref="IWarehouseRepository"/>.
        /// </summary>
        /// <param name="warehouseId"></param>
        /// <returns>An registered <see cref="Warehouse"/> object with a random ID.</returns>
        public Warehouse GetRegisteredWarehouse()
        {
            return GetRegisteredWarehouse(RandomStringGenerator.GenerateRandomString(3));
        }
    }
}
