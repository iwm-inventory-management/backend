﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_Core.Usecases.Warehouses;
using IWM_IM_Tests.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.UseCases
{
    public class WarehouseUsecases
    {
        private IWarehouseRepository _warehouseRepository;

        public WarehouseUsecases()
        {
            _warehouseRepository = new FakeWarehouseRepository();
        }

        [Fact]
        public void CreatedWarehouseIsSaved()
        {
            string warehouseId = "MAG";

            Warehouse warehouse = new Warehouse(warehouseId);
            CreateWarehouseUsecase createWarehouseUsecase = new CreateWarehouseUsecase(_warehouseRepository);
            createWarehouseUsecase.Execute(warehouse);

            Warehouse? retrievedWarehouse = _warehouseRepository.Get(warehouseId);

            Assert.NotNull(retrievedWarehouse);
            Assert.Equal(warehouseId, retrievedWarehouse.Id);
        }

        [Fact]
        public void CannotCreateWarehouseWithEmptyId()
        {
            string warehouseId = "";

            Warehouse warehouse = new Warehouse(warehouseId);
            CreateWarehouseUsecase createWarehouseUsecase = new CreateWarehouseUsecase(_warehouseRepository);
            Assert.Throws<Exception>(() => createWarehouseUsecase.Execute(warehouse));
        }

        [Fact]
        public void CannotCreateTwoWarehousesWithSameID()
        {
            string warehouseId = "MAG";

            Warehouse warehouse = new Warehouse(warehouseId);
            CreateWarehouseUsecase createWarehouseUsecase = new CreateWarehouseUsecase(_warehouseRepository);

            // Create a first warehouse with the given ID.
            createWarehouseUsecase.Execute(warehouse);

            // Creating a second warehouse with same ID must throw an Exception.            
            Assert.Throws<Exception>(() => createWarehouseUsecase.Execute(warehouse));
        }

        [Fact]
        public void AllSavedWarehousesCanBeRetrieved()
        {
            string warehouseId_1 = "MAG";
            string warehouseId_2 = "EXP";
            string warehouseId_3 = "REA";

            CreateWarehouseUsecase createWarehouseUsecase = new CreateWarehouseUsecase(_warehouseRepository);
            createWarehouseUsecase.Execute(new Warehouse(warehouseId_1));
            createWarehouseUsecase.Execute(new Warehouse(warehouseId_2));
            createWarehouseUsecase.Execute(new Warehouse(warehouseId_3));

            ListAllWarehousesUsecase listAllWarehousesUsecase = new ListAllWarehousesUsecase(_warehouseRepository);
            List<Warehouse> retrievedWarehouses = listAllWarehousesUsecase.Execute();

            Assert.Equal(3, retrievedWarehouses.Count);
            Assert.NotNull(() => retrievedWarehouses.SingleOrDefault(w => w.Id == warehouseId_1));
            Assert.NotNull(() => retrievedWarehouses.SingleOrDefault(w => w.Id == warehouseId_2));
            Assert.NotNull(() => retrievedWarehouses.SingleOrDefault(w => w.Id == warehouseId_3));
        }
    }
}
