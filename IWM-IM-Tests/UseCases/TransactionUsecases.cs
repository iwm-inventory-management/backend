﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Stocks;
using IWM_IM_Core.Usecases.Transactions;
using IWM_IM_Core.ValueObjects;
using IWM_IM_Tests.Double;
using IWM_IM_Tests.ObjectMothers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.UseCases
{
    public class TransactionUsecases
    {
        private IWarehouseRepository _warehouseRepository;
        private ILocationRepository _locationRepository;
        private IArticleRepository _articleRepository;
        private ITransactionRepository _transactionRepository;

        private LocationMother _locationMother;
        private ArticleMother _articleMother;

        public TransactionUsecases()
        {
            _warehouseRepository = new FakeWarehouseRepository();
            _locationRepository = new FakeLocationRepository();
            _articleRepository = new FakeArticleRepository();
            _transactionRepository = new FakeTransactionRepository();

            _locationMother = new LocationMother(_warehouseRepository, _locationRepository);
            _articleMother = new ArticleMother(_articleRepository);
        }

        [Fact]
        public void CreateTransactionIsSaved()
        {
            Location location = _locationMother.GetRegisteredLocationInRegisteredWarehouse();
            Article article = _articleMother.GetRegisteredArticle();

            EMovementType transactionType = EMovementType.Input;
            int transactionQuantity = 3;            
            User transactionUser = new User("userid");
            DateTime transactionDatetime = DateTime.Now;

            var transaction = new Transaction(transactionType, article, location, transactionQuantity, transactionUser, transactionDatetime);

            var createTransactionUsecase = new CreateTransactionUsecase(_transactionRepository, _locationRepository, _articleRepository);
            createTransactionUsecase.Execute(transaction);

            List<Transaction> transactionsList = _transactionRepository.GetAllOnLocation(location, "", "");

            Assert.Equal(1, transactionsList.Count);
            Transaction retrievedTransaction = transactionsList[0];

            Assert.Equal(transactionType, retrievedTransaction.Type);
            Assert.Equal(article.Id, retrievedTransaction.Article.Id);
            Assert.Equal(transactionQuantity, retrievedTransaction.Quantity);
            Assert.Equal(location.Id, retrievedTransaction.Location.Id);
            Assert.Equal(transactionUser.Id, retrievedTransaction.User.Id);
            Assert.Equal(transactionDatetime, retrievedTransaction.Date);
        }

        [Fact]
        public void AllTransactionsOnLocationAreRetrieved()
        {
            Location location = _locationMother.GetRegisteredLocationInRegisteredWarehouse();
            Article article_A = _articleMother.GetRegisteredArticle();
            Article article_B = _articleMother.GetRegisteredArticle();

            User user_A = new User("UserA");
            User user_B = new User("UserB");

            var transaction_1 = new Transaction(EMovementType.Input, article_A, location, 20, user_A, DateTime.Now);
            var transaction_2 = new Transaction(EMovementType.Adjust, article_A, location, -5, user_B, DateTime.Now);
            var transaction_3 = new Transaction(EMovementType.Output, article_A, location, -10, user_A, DateTime.Now);
            var transaction_4 = new Transaction(EMovementType.Adjust, article_B, location, 20, user_B, DateTime.Now);
            var transaction_5 = new Transaction(EMovementType.Input, article_A, location, 20, user_A, DateTime.Now);
            var transaction_6 = new Transaction(EMovementType.Output, article_A, location, -20, user_A, DateTime.Now);

            var createTransactionUsecase = new CreateTransactionUsecase(_transactionRepository, _locationRepository, _articleRepository);
            createTransactionUsecase.Execute(transaction_1);
            createTransactionUsecase.Execute(transaction_2);
            createTransactionUsecase.Execute(transaction_3);
            createTransactionUsecase.Execute(transaction_4);
            createTransactionUsecase.Execute(transaction_5);
            createTransactionUsecase.Execute(transaction_6);

            List<Transaction> transactionsList = _transactionRepository.GetAllOnLocation(location, "", "");

            Assert.Equal(6, transactionsList.Count);
        }

        [Fact]
        public void GetStockOfArticleOnLocation()
        {
            Location location = _locationMother.GetRegisteredLocationInRegisteredWarehouse();
            Article article_A = _articleMother.GetRegisteredArticle();
            Article article_B = _articleMother.GetRegisteredArticle();

            User user_A = new User("UserA");
            User user_B = new User("UserB");

            var transaction_1 = new Transaction(EMovementType.Input, article_A, location, 20, user_A, DateTime.Now);
            var transaction_5 = new Transaction(EMovementType.Input, article_B, location, 20, user_A, DateTime.Now);
            var transaction_2 = new Transaction(EMovementType.Adjust, article_A, location, -5, user_B, DateTime.Now);
            var transaction_6 = new Transaction(EMovementType.Output, article_B, location, -20, user_A, DateTime.Now);
            var transaction_3 = new Transaction(EMovementType.Output, article_A, location, -10, user_A, DateTime.Now);
            var transaction_4 = new Transaction(EMovementType.Adjust, article_B, location, 20, user_B, DateTime.Now);
     
            var createTransactionUsecase = new CreateTransactionUsecase(_transactionRepository, _locationRepository, _articleRepository);
            createTransactionUsecase.Execute(transaction_1);
            createTransactionUsecase.Execute(transaction_2);
            createTransactionUsecase.Execute(transaction_3);
            createTransactionUsecase.Execute(transaction_4);
            createTransactionUsecase.Execute(transaction_5);
            createTransactionUsecase.Execute(transaction_6);

            var getStockOfArticleOnLocationUsecase = new GetStockOfOneArticleOnLocation(_transactionRepository);
            Stock stockOfArticle_A = getStockOfArticleOnLocationUsecase.Execute(location, article_A);
            Stock stockOfArticle_B = getStockOfArticleOnLocationUsecase.Execute(location, article_B);

            Assert.Equal(5, stockOfArticle_A.Quantity);
            Assert.Equal(20, stockOfArticle_B.Quantity);
        }

        [Fact]
        public void GetAllStocksInLocation()
        {
            Location location = _locationMother.GetRegisteredLocationInRegisteredWarehouse();
            Article article_A = _articleMother.GetRegisteredArticle();
            Article article_B = _articleMother.GetRegisteredArticle();

            User user_A = new User("UserA");
            User user_B = new User("UserB");

            var transaction_1 = new Transaction(EMovementType.Input, article_A, location, 20, user_A, DateTime.Now);
            var transaction_5 = new Transaction(EMovementType.Input, article_B, location, 20, user_A, DateTime.Now);
            var transaction_2 = new Transaction(EMovementType.Adjust, article_A, location, -5, user_B, DateTime.Now);
            var transaction_6 = new Transaction(EMovementType.Output, article_B, location, -20, user_A, DateTime.Now);
            var transaction_3 = new Transaction(EMovementType.Output, article_A, location, -10, user_A, DateTime.Now);
            var transaction_4 = new Transaction(EMovementType.Adjust, article_B, location, 20, user_B, DateTime.Now);

            var createTransactionUsecase = new CreateTransactionUsecase(_transactionRepository, _locationRepository, _articleRepository);
            createTransactionUsecase.Execute(transaction_1);
            createTransactionUsecase.Execute(transaction_2);
            createTransactionUsecase.Execute(transaction_3);
            createTransactionUsecase.Execute(transaction_4);
            createTransactionUsecase.Execute(transaction_5);
            createTransactionUsecase.Execute(transaction_6);

            var getStockOfArticleOnLocationUsecase = new GetAllStocksOnLocation(_transactionRepository);
            List<Stock> stockOfArticle = getStockOfArticleOnLocationUsecase.Execute(location);
        }

    }
}
