﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_Core.Usecases.Warehouses;
using IWM_IM_Tests.Double;
using IWM_IM_Tests.ObjectMothers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.UseCases
{
    public class LocationUsecases
    {
        private ILocationRepository _locationRepository;
        private IWarehouseRepository _warehouseRepository;
        private WarehouseMother _warehouseMother;

        public LocationUsecases()
        {
            _locationRepository = new FakeLocationRepository();
            _warehouseRepository = new FakeWarehouseRepository();
            _warehouseMother = new WarehouseMother(_warehouseRepository);
        }

        [Fact]
        public void CreatedLocationIsSaved()
        {
            string warehouseId = "MAG";
            string locationName = "A-02-02-03";

            Warehouse warehouse = _warehouseMother.GetRegisteredWarehouse(warehouseId);

            Location location = new Location(locationName, warehouse);
            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);
            createLocationUsecase.Execute(location);

            Location? retrievedLocation = _locationRepository.GetByNameAndWarehouseId(locationName, warehouseId);
            Assert.NotNull(retrievedLocation);
            Assert.Equal(location.Id.ToString(), retrievedLocation.Id.ToString());
            Assert.Equal(location.Name, retrievedLocation.Name);
            Assert.Equal(warehouseId, retrievedLocation.Warehouse.Id);
        }

        [Fact]
        public void CannotCreateLocationWithEmptyName()
        {
            string warehouseId = "MAG";
            string locationName = "";

            Warehouse warehouse = _warehouseMother.GetRegisteredWarehouse(warehouseId);

            Location location = new Location(locationName, warehouse);

            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);
            Assert.Throws<Exception>(() => createLocationUsecase.Execute(location));
        }

        [Fact]
        public void CannotCreateLocationWithInexistingWarehouse()
        {
            string warehouseId = "MAG";
            string locationName = "A-02-02-03";

            Warehouse warehouse = new Warehouse(warehouseId);
            Location location = new Location(locationName, warehouse);
            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);

            Assert.Throws<Exception>(() => createLocationUsecase.Execute(location));
        }

        [Fact]
        public void CannotCreateLocationWithNullWarehouse()
        {
            string locationName = "A-02-02-03";

            Location location = new Location(locationName, null);

            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);
            Assert.Throws<Exception>(() => createLocationUsecase.Execute(location));
        }

        [Fact]
        public void CannotCreateTwoLocationWithSameNameInSameWarehouse()
        {
            string warehouseId = "MAG";
            string locationName = "A-02-02-03";

            Warehouse warehouse = _warehouseMother.GetRegisteredWarehouse(warehouseId);

            Location location = new Location(locationName, warehouse);
            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);

            // Create a first location with the given ID in the same Warehouse.
            createLocationUsecase.Execute(location);

            // Creating a second location with same ID and same Warehouse must throw an Exception.
            Assert.Throws<Exception>(() => createLocationUsecase.Execute(location));
        }

        [Fact]
        public void AllLocationsOfWarehouseAreRetrieved()
        {
            string warehouseId = "MAG";
            Warehouse warehouse = _warehouseMother.GetRegisteredWarehouse(warehouseId);

            string locationName_1 = "A-02-02-01";
            string locationName_2 = "A-02-02-02";
            string locationName_3 = "A-02-02-03";

            CreateLocationUsecase createLocationUsecase = new CreateLocationUsecase(_locationRepository, _warehouseRepository);
            createLocationUsecase.Execute(new Location(locationName_1, warehouse));
            createLocationUsecase.Execute(new Location(locationName_2, warehouse));
            createLocationUsecase.Execute(new Location(locationName_3, warehouse));

            ListAllLocationsInWarehouse listAllLocationsInWarehouse = new ListAllLocationsInWarehouse(_locationRepository, _warehouseRepository);
            List<Location> retrievedLocations = listAllLocationsInWarehouse.Execute(warehouse);

            Assert.Equal(3, retrievedLocations.Count);
            Assert.NotNull(() => retrievedLocations.SingleOrDefault(l => l.Name == locationName_1));
            Assert.NotNull(() => retrievedLocations.SingleOrDefault(l => l.Name == locationName_2));
            Assert.NotNull(() => retrievedLocations.SingleOrDefault(l => l.Name == locationName_3));
        }
    }
}
