﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Articles;
using IWM_IM_Tests.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.UseCases
{
    public class ArticleUsecases
    {
        private IArticleRepository _articleRepository;

        public ArticleUsecases()
        {
            _articleRepository = new FakeArticleRepository();
        }

        [Fact]
        public void CreatedArticleIsSaved()
        {
            string articleId = "E7495";

            Article article = new Article(articleId);
            CreateArticleUsecase createArticleUsecase = new CreateArticleUsecase(_articleRepository);
            createArticleUsecase.Execute(article);

            Article? retrievedArticle = _articleRepository.GetById(articleId);

            Assert.NotNull(retrievedArticle);
            Assert.Equal(articleId, retrievedArticle.Id);
        }

        [Fact]
        public void CannotCreateTwoArticlesWithSameId()
        {
            string articleId = "E7495";

            Article article = new Article(articleId);
            CreateArticleUsecase createArticleUsecase = new CreateArticleUsecase(_articleRepository);
            createArticleUsecase.Execute(article);

            Assert.Throws<Exception>(() => createArticleUsecase.Execute(article));
        }

        [Fact]
        public void CannotCreateArticleWithEmptyId()
        {
            string articleId = "";

            Article article = new Article(articleId);
            CreateArticleUsecase createArticleUsecase = new CreateArticleUsecase(_articleRepository);

            Assert.Throws<Exception>(() => createArticleUsecase.Execute(article));
        }

        [Fact]
        public void AllCreatedArticleAreRetrieved()
        {
            var article_1 = new Article("ABC01");
            var article_2 = new Article("ABC02");
            var article_3 = new Article("ABC03");

            CreateArticleUsecase createArticleUsecase = new CreateArticleUsecase(_articleRepository);
            createArticleUsecase.Execute(article_1);
            createArticleUsecase.Execute(article_2);
            createArticleUsecase.Execute(article_3);

            ListAllArticlesUsecase listAllArticlesUsecase = new ListAllArticlesUsecase(_articleRepository);
            List<Article> retrievedArticles = listAllArticlesUsecase.Execute();

            Assert.Equal(3, retrievedArticles.Count);
            Assert.Equal("ABC01", retrievedArticles[0].Id);
            Assert.Equal("ABC02", retrievedArticles[1].Id);
            Assert.Equal("ABC03", retrievedArticles[2].Id);
        }
    }
}
