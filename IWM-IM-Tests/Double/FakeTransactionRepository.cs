﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.Double
{
    internal class FakeTransactionRepository : ITransactionRepository
    {
        private List<Transaction> _transactionsList;

        public FakeTransactionRepository()
        {
            _transactionsList = new List<Transaction>();
        }

        public void Create(Transaction transaction)
        {
            var clonedTransaction = new Transaction(
                transaction.Type,
                new Article(transaction.Article.Id),
                new Location(transaction.Location.Id, transaction.Location.Name, new Warehouse(transaction.Location.Warehouse.Id)),
                transaction.Quantity,
                new User(transaction.User.Id),
                transaction.Date
                );

            _transactionsList.Add(clonedTransaction);
        }

        public List<Transaction> GetAllOnLocation(Location location, string periodStart, string periodEnd)
        {
            DateOnly tempMinDateOnly = string.IsNullOrWhiteSpace(periodStart) ? DateOnly.MinValue : DateOnly.Parse(periodStart);
            DateOnly tempMaxDateOnly = string.IsNullOrWhiteSpace(periodEnd) ? DateOnly.MaxValue : DateOnly.Parse(periodEnd);

            DateTime minimalDateTime = new DateTime(tempMinDateOnly.Year, tempMinDateOnly.Month, tempMinDateOnly.Day);
            DateTime maximalDateTime = new DateTime(tempMaxDateOnly.Year, tempMaxDateOnly.Month, tempMaxDateOnly.Day);

            List<Transaction> transactions = new List<Transaction>();
            foreach(var transaction in _transactionsList)
            {
                if (transaction.Location.Id == location.Id)
                    if (transaction.Date <= maximalDateTime && transaction.Date >= minimalDateTime)
                        transactions.Add(transaction);
            }
            return transactions;
        }

        public List<Transaction> GetAllOnLocationForOneArticle(Location location, Article article, string periodStart, string periodEnd)
        {
            DateOnly tempMinDateOnly = string.IsNullOrWhiteSpace(periodStart) ? DateOnly.MinValue : DateOnly.Parse(periodStart);
            DateOnly tempMaxDateOnly = string.IsNullOrWhiteSpace(periodEnd) ? DateOnly.MaxValue : DateOnly.Parse(periodEnd);

            DateTime minimalDateTime = new DateTime(tempMinDateOnly.Year, tempMinDateOnly.Month, tempMinDateOnly.Day);
            DateTime maximalDateTime = new DateTime(tempMaxDateOnly.Year, tempMaxDateOnly.Month, tempMaxDateOnly.Day);

            List<Transaction> transactions = new List<Transaction>();
            foreach (var transaction in _transactionsList)
            {
                if (transaction.Location.Id == location.Id && transaction.Article.Id == article.Id)
                    if (transaction.Date <= maximalDateTime && transaction.Date >= minimalDateTime)
                        transactions.Add(transaction);
            }
            return transactions;
        }

        public List<Transaction> GetAllOnWarehouse(Warehouse warehouse)
        {
            List<Transaction> transactions = new List<Transaction>();
            foreach (var transaction in _transactionsList)
            {
                if (transaction.Location.Warehouse.Id == warehouse.Id)
                    transactions.Add(transaction);
            }
            return transactions;
        }

        public List<Transaction> GetAllOnWarehouseForOneArticle(Warehouse warehouse, Article article)
        {
            List<Transaction> transactions = new List<Transaction>();
            foreach (var transaction in _transactionsList)
            {
                if (transaction.Location.Warehouse.Id == warehouse.Id && transaction.Article.Id == article.Id)
                    transactions.Add(transaction);
            }
            return transactions;
        }

        public List<Article> GetAllArticlesOnLocation(Location location)
        {
            List<Article> articles = new List<Article>();
            foreach (var transaction in _transactionsList)
            {
                if (articles.FirstOrDefault(a => a.Id == transaction.Article.Id) == null && transaction.Location.Id == location.Id)
                {
                    articles.Add(new Article(transaction.Article.Id));
                }
            }
            return articles;
        }

        public Transaction? GetLatestInventoryOnLocationForOneArticle(Location location, Article article)
        {
            Transaction latestInventory = null;

            foreach (var transaction in _transactionsList)
            {
                if (latestInventory == null || ( latestInventory.Type == EMovementType.Inventory && latestInventory.Date < transaction.Date) )
                    latestInventory = transaction;
            }

            return latestInventory;
        }

        public List<Article> GetAllArticlesInWarehouse(Warehouse warehouse)
        {
            List<Article> articles = new List<Article>();
            foreach (var transaction in _transactionsList)
            {
                if (articles.FirstOrDefault(a => a.Id == transaction.Article.Id) == null && transaction.Warehouse.Id == warehouse.Id)
                {
                    articles.Add(new Article(transaction.Article.Id));
                }
            }
            return articles;
        }

        public Transaction? GetLatestInventoryAtDateOnLocationForOneArticle(Location location, Article article, string datetime)
        {
            throw new NotImplementedException();
        }
    }
}
