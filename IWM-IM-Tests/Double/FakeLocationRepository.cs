﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.Double
{
    internal class FakeLocationRepository : ILocationRepository
    {
        private List<Location> _locationsList { get; set; }

        public FakeLocationRepository()
        {
            _locationsList = new List<Location>();
        }

        public void Create(Location location)
        {
            Location clonedLocation = new Location(location.Id, location.Name, new Warehouse(location.Warehouse.Id));
            _locationsList.Add(clonedLocation);
        }

        public Location? GetByNameAndWarehouseId(string locationName, string WarehouseId)
        {
            Location? retrievedLocation = _locationsList.SingleOrDefault(
                l => (l.Name == locationName && l.Warehouse.Id == WarehouseId)
            );

            if (retrievedLocation == null)
                return null;

            return new Location(retrievedLocation.Id, retrievedLocation.Name, retrievedLocation.Warehouse);
        }

        public List<Location> GetAllByWarehouseId(string warehouseId)
        {
            List<Location> allLocationInGivenWarehouse = new List<Location>();
            foreach (var location in _locationsList)
            {
                if (location.Warehouse.Id == warehouseId)
                    allLocationInGivenWarehouse.Add(location);
            }
            return allLocationInGivenWarehouse;
        }
    }
}
