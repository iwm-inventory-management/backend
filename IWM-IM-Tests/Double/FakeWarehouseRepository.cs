﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.Double
{
    internal class FakeWarehouseRepository : IWarehouseRepository
    {
        private List<Warehouse> _warehousesList { get; set; }

        public FakeWarehouseRepository()
        {
            _warehousesList = new List<Warehouse>();
        }

        public void AddLocationToWarehouse(Warehouse warehouse, Location location)
        {
            throw new NotImplementedException();
        }

        public void Create(Warehouse warehouse)
        {
            var clonedWarehouse = new Warehouse(warehouse.Id);

            _warehousesList.Add(clonedWarehouse);
        }

        public void Delete(string id)
        {
            bool warehouseIsDeleted = false;
            int warehouseIndex = 0;
            while (!warehouseIsDeleted && warehouseIndex < _warehousesList.Count)
            {
                Warehouse currentWarehouse = _warehousesList[warehouseIndex];
                if (currentWarehouse.Id == id)
                {
                    _warehousesList.RemoveAt(warehouseIndex);
                    warehouseIsDeleted = true;
                }                    
            }
        }

        public Warehouse? Get(string id)
        {
            Warehouse? retrievedWarehouse = _warehousesList.SingleOrDefault(w => w.Id == id);

            if (retrievedWarehouse == null)
                return null;

            return new Warehouse(retrievedWarehouse.Id);
        }

        public void RemoveLocationFromWarehouse(Warehouse warehouse, Location location)
        {
            throw new NotImplementedException();
        }

        public void Update(Warehouse warehouse)
        {
            Delete(warehouse.Id);
            Create(warehouse);
        }

        public List<Warehouse> GetAll()
        {
            List<Warehouse> warehouseList = new List<Warehouse>();
            foreach(var warehouse in _warehousesList)
            {
                warehouseList.Add(new Warehouse(warehouse.Id));
            }
            return warehouseList;
        }
    }
}
