﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests.Double
{
    internal class FakeArticleRepository : IArticleRepository
    {
        private List<Article> _articlesList { get; set; }

        public FakeArticleRepository()
        {
            _articlesList = new List<Article>();
        }

        public void Create(Article article)
        {
            Article clonedArticle = new Article(article.Id);
            _articlesList.Add(clonedArticle);
        }

        public Article? GetById(string articleId)
        {
            Article? retrievedArticle = _articlesList.SingleOrDefault(a => a.Id == articleId);

            if (retrievedArticle == null)
                return null;

            return new Article(retrievedArticle.Id);
        }

        public List<Article> GetAll()
        {
            List<Article> retrievedArticles = new List<Article>();
            foreach(var article in _articlesList)
            {
                retrievedArticles.Add(new Article(article.Id));
            }
            return retrievedArticles;
        }
    }
}
