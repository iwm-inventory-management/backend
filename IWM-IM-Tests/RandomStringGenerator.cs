﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Tests
{
    internal static class RandomStringGenerator
    {
        private static readonly Random _random = new Random();

        public static string GenerateRandomString(int length)
        {
            var builder = new StringBuilder(length);

            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < length; i++)
            {
                var @char = (char)_random.Next('a', 'a' + lettersOffset);
                builder.Append(@char);
            }

            return builder.ToString();
        }
    }
}
