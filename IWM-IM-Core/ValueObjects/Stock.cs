﻿using IWM_IM_Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.ValueObjects
{
    public struct Stock
    {
        public Warehouse Warehouse { get; }
        public Location Location { get; }
        public Article Article { get; }
        public int Quantity { get; }

        public Stock(Warehouse warehouse, Location location, Article article, int quantity)
        {
            Warehouse = warehouse;
            Location = location;
            Article = article;
            Quantity = quantity;
        }
    }
}
