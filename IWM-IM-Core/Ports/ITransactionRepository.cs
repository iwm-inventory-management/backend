﻿using IWM_IM_Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Ports
{
    public interface ITransactionRepository
    {
        public void Create(Transaction transaction);
        public List<Transaction> GetAllOnLocation(Location location, string periodStart, string periodEnd);
        public List<Transaction> GetAllOnLocationForOneArticle(Location location, Article article, string periodStart, string periodEnd);
        public List<Transaction> GetAllOnWarehouse(Warehouse warehouse);
        public List<Transaction> GetAllOnWarehouseForOneArticle(Warehouse warehouse, Article article);
        public List<Article> GetAllArticlesOnLocation(Location location);
        public List<Article> GetAllArticlesInWarehouse(Warehouse warehouse);
        public Transaction? GetLatestInventoryOnLocationForOneArticle(Location location, Article article);
        public Transaction? GetLatestInventoryAtDateOnLocationForOneArticle(Location location, Article article, string datetime);
    }
}
