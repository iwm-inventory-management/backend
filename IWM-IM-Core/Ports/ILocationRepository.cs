﻿using IWM_IM_Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Ports
{
    public interface ILocationRepository
    {
        public Location? GetByNameAndWarehouseId(string locationName, string warehouseId);
        public List<Location> GetAllByWarehouseId(string warehouseId);
        public void Create(Location location);
    }
}
