﻿using IWM_IM_Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Ports
{
    public interface IWarehouseRepository
    {
        void Update(Warehouse warehouse);
        Warehouse? Get(string id);

        public void Create(Warehouse Warehouse);

        public List<Warehouse> GetAll();

        public void Delete(string id);

        public void AddLocationToWarehouse(Warehouse warehouse, Location location);
        public void RemoveLocationFromWarehouse(Warehouse warehouse, Location location);
    }
}
