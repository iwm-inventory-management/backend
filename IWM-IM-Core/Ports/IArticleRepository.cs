﻿using IWM_IM_Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Ports
{
    public interface IArticleRepository
    {
        public void Create(Article article);
        public Article? GetById(string articleId);
        public List<Article> GetAll();
    }
}
