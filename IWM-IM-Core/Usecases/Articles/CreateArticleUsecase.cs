﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Articles
{
    public class CreateArticleUsecase
    {
        private IArticleRepository _articleRepository;

        public CreateArticleUsecase(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public void Execute(Article article)
        {
            if (string.IsNullOrWhiteSpace(article.Id))
                throw new Exception("Cannot create an article with empty Id.");

            Article? articleWithSameId = _articleRepository.GetById(article.Id);

            if (articleWithSameId != null)
                throw new Exception("An article with the same ID already exists.");

            _articleRepository.Create(article);
        }
    }
}
