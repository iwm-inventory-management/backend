﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Articles
{
    public class ListAllArticlesUsecase
    {
        private IArticleRepository _articleRepository;

        public ListAllArticlesUsecase(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public List<Article> Execute()
        {
            return _articleRepository.GetAll();
        }
    }
}
