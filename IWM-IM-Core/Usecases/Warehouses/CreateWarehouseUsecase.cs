﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;

namespace IWM_IM_Core.Usecases.Warehouses
{
    public class CreateWarehouseUsecase
    {
        private IWarehouseRepository _warehouseRepository;

        public CreateWarehouseUsecase(IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
        }

        public void Execute(Warehouse warehouse)
        {
            if (string.IsNullOrWhiteSpace(warehouse.Id))
                throw new Exception("Cannot create Warehouse with empty name.");

            Warehouse? warehouseWithSameId = _warehouseRepository.Get(warehouse.Id);

            if (warehouseWithSameId != null)
                throw new Exception("A warehouse with the same ID already exists.");

            _warehouseRepository.Create(warehouse);
        }
    }
}
