﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;

namespace IWM_IM_Core.Usecases.Locations
{
    public class CreateLocationUsecase
    {
        private ILocationRepository _locationRepository;
        private IWarehouseRepository _warehouseRepository;

        public CreateLocationUsecase(ILocationRepository locationRepository, IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _locationRepository = locationRepository;
        }

        public void Execute(Location location)
        {
            if (string.IsNullOrWhiteSpace(location.Name))
                throw new Exception("Cannot create Location with an empty Name.");

            if (location.Warehouse == null)
                throw new Exception("Cannot create Location with a null Warehouse.");

            Warehouse? retrievedWarehouse = _warehouseRepository.Get(location.Warehouse.Id);
            if (retrievedWarehouse == null)
                throw new Exception("Cannot create Location with an unregistered Warehouse.");

            Location? locationWithSameId = _locationRepository.GetByNameAndWarehouseId(location.Name, location.Warehouse.Id);

            if (locationWithSameId != null)
                throw new Exception("A location with the same name already exists in that same Warehouse.");

            _locationRepository.Create(location);
        }
    }
}
