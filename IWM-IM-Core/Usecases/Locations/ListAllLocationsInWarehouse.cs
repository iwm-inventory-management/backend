﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Locations
{
    public class ListAllLocationsInWarehouse
    {
        private ILocationRepository _locationRepository;
        private IWarehouseRepository _warehouseRepository;

        public ListAllLocationsInWarehouse(ILocationRepository locationRepository, IWarehouseRepository warehouseRepository)
        {
            _warehouseRepository = warehouseRepository;
            _locationRepository = locationRepository;
        }

        public List<Location> Execute(Warehouse warehouse)
        {
            Warehouse? retrievedWarehouse = _warehouseRepository.Get(warehouse.Id);
            if (retrievedWarehouse == null)
                throw new Exception("Cannot create Location with an inexisting Warehouse.");

            return _locationRepository.GetAllByWarehouseId(warehouse.Id);
        }
    }
}
