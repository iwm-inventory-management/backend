﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using IWM_IM_Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Stocks
{
    public class GetAllStocksOnLocation
    {
        private ITransactionRepository _transactionRepository;

        public GetAllStocksOnLocation(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public List<Stock> Execute(Location location)
        {
            List<Stock> result = new List<Stock>();

            // Retrieve all articles on Location
            List<Article> articles = _transactionRepository.GetAllArticlesOnLocation(location);
            //if (articles.Count == 0)
            //    throw new Exception("No article is stored in this Location");

            foreach (Article article in articles)
            {
                // Retrieved latest inventory
                Transaction? latestInventory = _transactionRepository.GetLatestInventoryOnLocationForOneArticle(location, article);

                int latestInventoryQuantity = 0;
                string latestInventoryDateAsString = "";
                DateTime latestInventoryDateTime = DateTime.MinValue;
                if (latestInventory != null)
                {
                    latestInventoryQuantity = latestInventory.Quantity;
                    latestInventoryDateAsString = $"{latestInventory.Date.Year:D4}-{latestInventory.Date.Month:D2}-{latestInventory.Date.Day:D2}";
                    latestInventoryDateTime = latestInventory.Date;
                }

                List<Transaction> transactions = _transactionRepository.GetAllOnLocationForOneArticle(location, article, latestInventoryDateAsString, "");

                int quantityOfArticleInLocation = latestInventoryQuantity;
                foreach (Transaction transaction in transactions) 
                {
                    if (transaction.Date >= latestInventoryDateTime && transaction.Type != EMovementType.Inventory)
                        quantityOfArticleInLocation += transaction.Quantity;
                }

                result.Add(new Stock(location.Warehouse, location, article, quantityOfArticleInLocation));
            }

            return result;
        }
    }
}
