﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using IWM_IM_Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Stocks
{
    public class GetStocksOfOneArticleInEachLocationsOfWarehouse
    {
        private ITransactionRepository _transactionRepository;
        private ILocationRepository _locationRepository;

        public GetStocksOfOneArticleInEachLocationsOfWarehouse(ITransactionRepository transactionRepository, ILocationRepository locationRepository)
        {
            _transactionRepository = transactionRepository;
            _locationRepository = locationRepository;
        }

        public List<Stock> Execute(Warehouse warehouse, Article article)
        {
            List<Stock> stocksInWarehouse = new List<Stock>();

            // Retrieve all articles and locations in the given Warehouse
            List<Article> articlesInWarehouse = _transactionRepository.GetAllArticlesInWarehouse(warehouse);
            List<Location> locationsInWarehouse = _locationRepository.GetAllByWarehouseId(warehouse.Id);

            foreach (Location location in locationsInWarehouse)
            {
                // Retrieved latest inventory
                Transaction? latestInventory = _transactionRepository.GetLatestInventoryOnLocationForOneArticle(location, article);

                int latestInventoryQuantity = 0;
                string latestInventoryDateAsString = "";
                DateTime latestInventoryDateTime = DateTime.MinValue;
                if (latestInventory != null)
                {
                    latestInventoryQuantity = latestInventory.Quantity;
                    latestInventoryDateAsString = $"{latestInventory.Date.Year:D4}-{latestInventory.Date.Month:D2}-{latestInventory.Date.Day:D2}";
                    latestInventoryDateTime = latestInventory.Date;
                }

                List<Transaction> transactions = _transactionRepository.GetAllOnLocationForOneArticle(location, article, latestInventoryDateAsString, "");

                int quantityOfArticleInLocation = latestInventoryQuantity;
                foreach (Transaction transaction in transactions)
                {
                    if (transaction.Date >= latestInventoryDateTime && transaction.Type != EMovementType.Inventory)
                        quantityOfArticleInLocation += transaction.Quantity;
                }

                stocksInWarehouse.Add(new Stock(location.Warehouse, location, article, quantityOfArticleInLocation));
            }

            return stocksInWarehouse;
        }
    }
}
