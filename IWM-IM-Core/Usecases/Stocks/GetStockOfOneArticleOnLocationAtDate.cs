﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using IWM_IM_Core.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Stocks
{
    public class GetStockOfOneArticleOnLocationAtDate
    {
        private ITransactionRepository _transactionRepository;

        public GetStockOfOneArticleOnLocationAtDate(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public Stock Execute(Location location, Article article, string dateTime)
        {
            // Check if article is stored in the given Location
            List<Article> articlesInLocation = _transactionRepository.GetAllArticlesOnLocation(location);
            if (articlesInLocation.SingleOrDefault(a => a.Id == article.Id) == null)
                throw new Exception("The given article is not stored in this Location");

            // Retrieve latest inventory of article in the given Location
            Transaction? latestInventory = _transactionRepository.GetLatestInventoryAtDateOnLocationForOneArticle(location, article, dateTime);

            int latestInventoryQuantity = 0;
            string latestInventoryDateAsString = "";
            DateTime latestInventoryDateTime = DateTime.MinValue;
            if (latestInventory != null)
            {
                latestInventoryQuantity = latestInventory.Quantity;
                latestInventoryDateAsString = $"{latestInventory.Date.Year:D4}-{latestInventory.Date.Month:D2}-{latestInventory.Date.Day:D2}";
                latestInventoryDateTime = latestInventory.Date;
            }

            List<Transaction> transactions = _transactionRepository.GetAllOnLocationForOneArticle(location, article, latestInventoryDateAsString, dateTime);

            int quantityOfArticleInLocation = latestInventoryQuantity;
            foreach (Transaction transaction in transactions)
            {
                if (transaction.Date >= latestInventoryDateTime && transaction.Type != EMovementType.Inventory)
                    quantityOfArticleInLocation += transaction.Quantity;
            }

            return new Stock(location.Warehouse, location, article, quantityOfArticleInLocation);
        }
    }
}
