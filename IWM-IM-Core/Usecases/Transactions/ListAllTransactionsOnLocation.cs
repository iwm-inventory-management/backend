﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Transactions
{
    public class ListAllTransactionsOnLocation
    {
        private ITransactionRepository _transactionRepository;

        public ListAllTransactionsOnLocation(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public List<Transaction> Execute(Location location, string periodStart, string periodEnd)
        {
            return _transactionRepository.GetAllOnLocation(location, periodStart, periodEnd);
        }
    }
}
