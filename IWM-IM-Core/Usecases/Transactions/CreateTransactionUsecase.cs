﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Usecases.Transactions
{
    public class CreateTransactionUsecase
    {
        private ITransactionRepository _transactionRepository;
        private ILocationRepository _locationRepository;
        private IArticleRepository _articleRepository;

        public CreateTransactionUsecase(ITransactionRepository transactionRepository, ILocationRepository locationRepository, IArticleRepository articleRepository)
        {
            _transactionRepository = transactionRepository;
            _locationRepository = locationRepository;
            _articleRepository = articleRepository;
        }

        public void Execute(Transaction transaction)
         {
            if (transaction.Quantity == 0)
                throw new Exception("Cannot create a Transaction without a Quantity.");

            Article? retrievedArticle = _articleRepository.GetById(transaction.Article.Id);
            if (retrievedArticle == null)
                throw new Exception("Cannot create a Transaction with an unregistered Article.");

            Location? retrievedLocation = _locationRepository.GetByNameAndWarehouseId(
                transaction.Location.Name, transaction.Location.Warehouse.Id
            );
            if (retrievedLocation == null)
                throw new Exception("Cannot create a Transaction with an unregistered Location.");

            _transactionRepository.Create(transaction);
        }
    }
}
