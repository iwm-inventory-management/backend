﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IWM_IM_Core.Enum;

namespace IWM_IM_Core.Entities
{
    public class Transaction
    {
        public EMovementType Type { get; }
        public Article Article { get; }
        public int Quantity { get; }
        public Warehouse Warehouse { get; }
        public Location Location { get; }
        public User User { get; }
        public DateTime Date { get; }

        public Transaction(EMovementType type, Article article, Location location, int quantity, User user, DateTime date)
        {
            Type = type;
            Article = article;
            Warehouse = location.Warehouse;
            Location = location;            
            Quantity = quantity;
            User = user;
            Date = date;
        }
    }
}
