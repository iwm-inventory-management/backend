﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Entities
{
    public class Location
    {
        public Guid Id { get; }
        public string Name { get; }
        public Warehouse Warehouse { get; }

        public Location(Guid id, string name, Warehouse warehouse)
        {
            Id = id;
            Name = name;
            Warehouse = warehouse;
        }

        public Location(string name, Warehouse warehouse)
        {
            Id = Guid.NewGuid();
            Name = name;
            Warehouse = warehouse;
        }
    }
}
