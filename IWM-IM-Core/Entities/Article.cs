﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Entities
{
    public  class Article
    {
        public string Id { get; }

        public Article(string id)
        {
            Id = id;
        }
    }
}
