﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_Core.Entities
{
    public class User
    {
        public string Id { get; }

        public User(string id)
        {
            Id = id;
        }
    }
}
