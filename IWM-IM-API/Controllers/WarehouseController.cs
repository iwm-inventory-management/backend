﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Warehouses;
using IWM_IM_SQL_Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace IWM_IM_API.Controllers
{    
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private IWarehouseRepository _warehouseRepository { get; }

        public WarehouseController(IConfiguration config)
        {
            string connectionString = SqlConnectionStringManager.GetInstance().GetConnectionString();
            MySqlConnection sqlConnectionString = new MySqlConnection(connectionString);

            _warehouseRepository = new SQLWarehouseRepository(sqlConnectionString, "Warehouses");
        }

        [HttpGet]
        [Route("api/warehouses")]
        public IActionResult GetAllWarehouses()
        {
            var listAllWarehousesUsecase = new ListAllWarehousesUsecase(_warehouseRepository);
            List<Warehouse> retrievedWarehouses = new List<Warehouse>();

            try
            {
                retrievedWarehouses = listAllWarehousesUsecase.Execute();
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            return Ok(retrievedWarehouses);
        }
    }
}
