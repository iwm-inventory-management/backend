﻿using IWM_IM_API.DataTransferObjects.OutputDTO;
using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_SQL_Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace IWM_IM_API.Controllers
{
    [ApiController]
    public class LocationController : ControllerBase
    {
        private ILocationRepository _locationRepository { get; }
        private IWarehouseRepository _warehouseRepository { get; }

        public LocationController(IConfiguration config)
        {
            string connectionString = SqlConnectionStringManager.GetInstance().GetConnectionString();
            MySqlConnection sqlConnectionString = new MySqlConnection(connectionString);

            _locationRepository = new SQLLocationRepository(sqlConnectionString, "Locations");
            _warehouseRepository = new SQLWarehouseRepository(sqlConnectionString, "Warehouses");
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations")]
        public IActionResult GetAllLocationsOfWarehouse([FromRoute] string warehouseId)
        {
            var warehouse = new Warehouse(warehouseId);

            var listAllLocationInWarehouseUsecase = new ListAllLocationsInWarehouse(_locationRepository, _warehouseRepository);
            List<Location> retrievedLocations = new List<Location>();

            try
            {
                retrievedLocations = listAllLocationInWarehouseUsecase.Execute(warehouse);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<LocationOutputDTO> resultList = new List<LocationOutputDTO>();

            foreach(var location in retrievedLocations)
                resultList.Add(new LocationOutputDTO(location));

            return Ok(resultList);
        }
    }   
}
