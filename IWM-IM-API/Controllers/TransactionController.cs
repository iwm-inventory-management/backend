﻿using IWM_IM_API.DataTransferObjects;
using IWM_IM_API.DataTransferObjects.InputDTO;
using IWM_IM_API.DataTransferObjects.OutputDTO;
using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_Core.Usecases.Transactions;
using IWM_IM_SQL_Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace IWM_IM_API.Controllers
{
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private ITransactionRepository _transactionRepository { get; }
        private ILocationRepository _locationRepository { get; }
        private IArticleRepository _articleRepository { get; }

        public TransactionController(IConfiguration config)
        {
            string connectionString = SqlConnectionStringManager.GetInstance().GetConnectionString();
            MySqlConnection sqlConnectionString = new MySqlConnection(connectionString);

            _transactionRepository = new SQLTransactionRepository(sqlConnectionString, "Transactions");
            _locationRepository = new SQLLocationRepository(sqlConnectionString, "Locations");
            _articleRepository = new SQLArticleRepository(sqlConnectionString, "Articles");
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations/{locationName}/transactions")]
        public IActionResult GetAllTransactionsInLocation([FromRoute] string warehouseId, [FromRoute] string locationName,
                                                          string? periodStart, string? periodEnd)
        {
            string minDate = string.IsNullOrWhiteSpace(periodStart) ? "0001-01-01" : periodStart;
            string maxDate = string.IsNullOrWhiteSpace(periodEnd) ? "2030-12-31" : periodEnd;

            var warehouse = new Warehouse(warehouseId);
            var location = _locationRepository.GetByNameAndWarehouseId(locationName, warehouseId);

            var listAllTransactionsOnLocationUsecase = new ListAllTransactionsOnLocation(_transactionRepository);
            List<Transaction> allTransactions = new List<Transaction>();

            try
            {
                allTransactions = listAllTransactionsOnLocationUsecase.Execute(location, minDate, maxDate);
            }
            catch(Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<TransactionOutputDTO> allTransactionsAsDTO = new List<TransactionOutputDTO>();
            foreach (var transaction in allTransactions)
            {
                allTransactionsAsDTO.Add(new TransactionOutputDTO(transaction));
            }

            return Ok(allTransactionsAsDTO);
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations/{locationName}/products/{productId}/transactions")]
        public IActionResult GetAllTransactionsOnProductInLocation([FromRoute] string warehouseId, [FromRoute] string locationName, [FromRoute] string productId,
                                                                   string? periodStart, string? periodEnd)
        {
            string minDate = string.IsNullOrWhiteSpace(periodStart) ? "0001-01-01" : periodStart;
            string maxDate = string.IsNullOrWhiteSpace(periodEnd) ? "2030-12-31" : periodEnd;

            var warehouse = new Warehouse(warehouseId);
            var location = _locationRepository.GetByNameAndWarehouseId(locationName, warehouseId);
            var article = new Article(productId);

            var listAllTransactionsOfArticleInLocation = new ListAllTransactionsOfOneArticleOnLocation(_transactionRepository);
            List<Transaction> allTransactions = new List<Transaction>();

            try
            {
                allTransactions = listAllTransactionsOfArticleInLocation.Execute(location, article, minDate, maxDate);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<TransactionOutputDTO> allTransactionsAsDTO = new List<TransactionOutputDTO>();
            foreach(var transaction in allTransactions)
            {
                allTransactionsAsDTO.Add(new TransactionOutputDTO(transaction));
            }

            return Ok(allTransactionsAsDTO);
        }

        [HttpPost]
        [Route("api/warehouses/{warehouseId}/locations/{locationName}/products/{productId}/transactions")]
        public IActionResult PostNewTransactionOnProductInLocation([FromRoute] string warehouseId, [FromRoute] string locationName, [FromRoute] string productId, TransactionInputDTO transactionDTO)
        {
            var warehouse = new Warehouse(warehouseId.ToUpper());
            var article = new Article(productId.ToUpper());
            var location = _locationRepository.GetByNameAndWarehouseId(locationName.ToUpper(), warehouseId.ToUpper());

            var user = new User(transactionDTO.User);
            int quantity = transactionDTO.Quantity;
            DateTime date = transactionDTO.Date;
            string movementTypeAsString = transactionDTO.Type;
            EMovementType movementType = EMovementType.Input;

            switch(movementTypeAsString.ToLower())
            {
                case "input":
                    movementType = EMovementType.Input;
                    break;
                case "output":
                    movementType = EMovementType.Output;
                    break;
                case "inventory":
                    movementType = EMovementType.Inventory;
                    break;
                case "adjust":
                    movementType = EMovementType.Adjust;
                    break;
                default:
                    return BadRequest(new { Message = "Invalid movement type." });
            }

            Transaction newTransaction = new Transaction(movementType, article, location, quantity, user, date);

            var createTransactionUsecase = new CreateTransactionUsecase(_transactionRepository, _locationRepository, _articleRepository);
            try
            {
                createTransactionUsecase.Execute(newTransaction);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            return Ok(new { Message = "Ok." });
        }
    }
}
