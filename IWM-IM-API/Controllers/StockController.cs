﻿using IWM_IM_API.DataTransferObjects.OutputDTO;
using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Stocks;
using IWM_IM_Core.Usecases.Transactions;
using IWM_IM_Core.ValueObjects;
using IWM_IM_SQL_Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using static Google.Protobuf.Reflection.SourceCodeInfo.Types;

namespace IWM_IM_API.Controllers
{
    [ApiController]
    public class StockController : ControllerBase
    {
        private ITransactionRepository _transactionRepository { get; }
        private ILocationRepository _locationRepository { get; }
        public StockController(IConfiguration config)
        {
            string connectionString = SqlConnectionStringManager.GetInstance().GetConnectionString();
            MySqlConnection sqlConnectionString = new MySqlConnection(connectionString);

            _transactionRepository = new SQLTransactionRepository(sqlConnectionString, "Transactions");
            _locationRepository = new SQLLocationRepository(sqlConnectionString, "Locations");
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/stocks")]
        public IActionResult GetAllStockOfWarehouse([FromRoute] string warehouseId)
        {
            var warehouse = new Warehouse(warehouseId);

            var listOfAllStockOfWarehouseUsecase = new GetAllStocksInWarehouse(_transactionRepository, _locationRepository);
            List<Stock> allStocks = new List<Stock>();

            try
            {
                allStocks = listOfAllStockOfWarehouseUsecase.Execute(warehouse);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<StockOutputDTO> allStockAsDTO = new List<StockOutputDTO>();
            foreach (var stock in allStocks)
            {
                allStockAsDTO.Add(new StockOutputDTO(stock));
            }

            return Ok(allStockAsDTO);
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations/{locationId}/stocks")]
        public IActionResult GetAllStockInLocation([FromRoute] string warehouseId, [FromRoute] string locationId)
        {
            var warehouse = new Warehouse(warehouseId);
            var location = _locationRepository.GetByNameAndWarehouseId(locationId, warehouseId);

            var listOfAllStockInLocationUsecase = new GetAllStocksOnLocation(_transactionRepository);
            List<Stock> allStocks = new List<Stock>();

            try
            {
                allStocks = listOfAllStockInLocationUsecase.Execute(location);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<StockOutputDTO> allStockAsDTO = new List<StockOutputDTO>();
            foreach (var stock in allStocks)
            {
                allStockAsDTO.Add(new StockOutputDTO(stock));
            }

            return Ok(allStockAsDTO);
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/products/{productId}/stocks")]
        public IActionResult GetStockOfProductInEachLocationOfWarehouse([FromRoute] string warehouseId, [FromRoute] string productId)
        {
            var warehouse = new Warehouse(warehouseId);
            var article = new Article(productId);

            var listOfStockOfProductInWarehouseUsecase = new GetStocksOfOneArticleInEachLocationsOfWarehouse(_transactionRepository, _locationRepository);
            List<Stock> allStocks = new List<Stock>();
            try
            {
                listOfStockOfProductInWarehouseUsecase.Execute(warehouse, article);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            List<StockOutputDTO> allStockAsDTO = new List<StockOutputDTO>();
            foreach (var stock in allStocks)
            {
                allStockAsDTO.Add(new StockOutputDTO(stock));
            }

            return Ok(allStockAsDTO);
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations/{locationId}/products/{productId}/stocks")]
        public IActionResult GetStockOfProductInLocation([FromRoute] string warehouseId, [FromRoute] string locationId, [FromRoute] string productId)
        {
            var warehouse = new Warehouse(warehouseId);
            var location = _locationRepository.GetByNameAndWarehouseId(locationId, warehouseId);
            var article = new Article(productId);

            var listOfStockOfProductInLocationUsecase = new GetStockOfOneArticleOnLocation(_transactionRepository);
            Stock stockOfProduct = new Stock();

            try
            {
                stockOfProduct = listOfStockOfProductInLocationUsecase.Execute(location, article);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            return Ok(new StockOutputDTO(stockOfProduct));
        }

        [HttpGet]
        [Route("api/warehouses/{warehouseId}/locations/{locationId}/products/{productId}/stocksAtDate")]
        public IActionResult GetStockOfProductInLocationAtDate([FromRoute] string warehouseId, [FromRoute] string locationId, [FromRoute] string productId, string? stockDate)
        {
            Regex dateRegex = new Regex(@"\d\d\d\d-\d\d-\d\d");
            if (string.IsNullOrWhiteSpace(stockDate) || !dateRegex.IsMatch(stockDate, 0))
            {
                return BadRequest(new { Message = "A date YYYY-MM-DD must be provided." });
            }

            var warehouse = new Warehouse(warehouseId);
            var location = _locationRepository.GetByNameAndWarehouseId(locationId, warehouseId);
            var article = new Article(productId);
            string datetime = $"{stockDate} 23:59:59";

            var stockOfProductAtDateUsecase = new GetStockOfOneArticleOnLocationAtDate(_transactionRepository);
            Stock stockOfProductInLocation = new Stock();

            try
            {
                stockOfProductInLocation = stockOfProductAtDateUsecase.Execute(location, article, datetime);
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }
            return Ok(new StockOutputDTO(stockOfProductInLocation));
        }
    }
}
