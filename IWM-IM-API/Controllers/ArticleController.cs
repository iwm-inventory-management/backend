﻿using IWM_IM_API.DataTransferObjects.OutputDTO;
using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using IWM_IM_Core.Usecases.Articles;
using IWM_IM_Core.Usecases.Locations;
using IWM_IM_SQL_Adapter;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace IWM_IM_API.Controllers
{
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private IArticleRepository _articleRepository;

        public ArticleController()
        {
            string connectionString = SqlConnectionStringManager.GetInstance().GetConnectionString();
            MySqlConnection sqlConnectionString = new MySqlConnection(connectionString);

            _articleRepository = new SQLArticleRepository(sqlConnectionString, "Articles");
        }

        [HttpGet]
        [Route("api/articles")]
        public IActionResult GetAllArticles()
        {
            var listAllArticlesUsecase = new ListAllArticlesUsecase(_articleRepository);
            List<Article> retrievedArticles = new List<Article>();
            try
            {
                retrievedArticles = listAllArticlesUsecase.Execute();
            }
            catch (Exception e)
            {
                return BadRequest(new { Message = e.Message });
            }

            return Ok(retrievedArticles);
        }
    }
}
