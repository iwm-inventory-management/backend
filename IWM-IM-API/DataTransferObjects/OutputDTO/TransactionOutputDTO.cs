﻿using IWM_IM_Core.Entities;
using System.Text.Json.Serialization;

namespace IWM_IM_API.DataTransferObjects.OutputDTO
{
    public class TransactionOutputDTO
    {
        public string? Type { get; }
        public string? Product { get; }
        public int? Quantity { get; }
        public string? Warehouse { get; }
        public string? Location { get; }
        public string? User { get; }
        public DateTime? Date { get; }

        public TransactionOutputDTO() { }

        public TransactionOutputDTO(Transaction transaction)
        {
            Type = transaction.Type.ToString();
            Product = transaction.Article.Id;
            Quantity = transaction.Quantity;
            Warehouse = transaction.Location.Warehouse.Id;
            Location = transaction.Location.Name;
            User = transaction.User.Id;
            Date = transaction.Date;
        }
    }
}
