﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.ValueObjects;

namespace IWM_IM_API.DataTransferObjects.OutputDTO
{
    public class StockOutputDTO
    {
        public string Warehouse { get; }
        public string Location { get; }
        public string Article { get; }
        public int Quantity { get; }

        public StockOutputDTO(Stock stock)
        {
            Warehouse = stock.Warehouse.Id;
            Location = stock.Location.Name;
            Article = stock.Article.Id;
            Quantity = stock.Quantity;
        }
    }
}
