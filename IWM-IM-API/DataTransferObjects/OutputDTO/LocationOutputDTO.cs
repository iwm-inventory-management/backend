﻿using IWM_IM_Core.Entities;

namespace IWM_IM_API.DataTransferObjects.OutputDTO
{
    public class LocationOutputDTO
    {
        public string Id { get; set; } = "";

        public LocationOutputDTO(Location location)
        {
            Id = location.Name;
        }
    }
}
