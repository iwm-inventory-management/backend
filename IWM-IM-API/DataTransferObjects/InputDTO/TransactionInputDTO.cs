﻿namespace IWM_IM_API.DataTransferObjects.InputDTO
{
    public class TransactionInputDTO
    {
        public string Type { get; set; } = "";
        public int Quantity { get; set; } = 0;
        public string User { get; set; } = "";
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
