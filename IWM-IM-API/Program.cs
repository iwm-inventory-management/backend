using IWM_IM_SQL_Adapter;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyHeader()
                  .AllowAnyMethod()
                  .AllowAnyOrigin();
        });
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Setting up connection string 
var connectionStringManager = SqlConnectionStringManager.GetInstance();
connectionStringManager.SetConnectionStringParameters(
    Environment.GetEnvironmentVariable("MYSQL_SERVER_NAME"),
    Environment.GetEnvironmentVariable("MYSQL_SERVER_PORT"),
    Environment.GetEnvironmentVariable("MYSQL_DB_NAME"),
    Environment.GetEnvironmentVariable("MYSQL_USER"),
    Environment.GetEnvironmentVariable("MYSQL_PASSWORD")
);


//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseCors();

app.Run();
