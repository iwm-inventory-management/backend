﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_SQL_Adapter
{
    public class SQLLocationRepository : ILocationRepository
    {
        private MySqlConnection _mySqlConnection { get; }
        private string _tableName { get; }

        public SQLLocationRepository(MySqlConnection mySqlConnection, string tableName)
        {
            _mySqlConnection = mySqlConnection;
            _tableName = tableName;
        }

        public void Create(Location location)
        {
            string sqlRequest = $"INSERT INTO {_tableName} (id, name, warehouse_id) VALUES (@id, @name, @warehouse_id)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@id", location.Id);
            sqlCommand.Parameters.AddWithValue("@name", location.Name);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", location.Warehouse.Id);

            try
            {
                _mySqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

        }

        public List<Location> GetAllByWarehouseId(string warehouseId)
        {
            List<Location> retrievedLocations = new List<Location>();

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE warehouse_id = @warehouse_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", warehouseId);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Guid id = dataReader.GetGuid("id");
                    string name = dataReader.GetString("name");
                    Warehouse warehouse = new Warehouse(dataReader.GetString("warehouse_id"));
                    retrievedLocations.Add(new Location(id, name, warehouse));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedLocations;
        }

        public Location? GetByNameAndWarehouseId(string locationName, string warehouseId)
        {
            Location? retrievedLocation = null;

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE name = @name AND warehouse_id = @warehouse_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@name", locationName);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", warehouseId);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Guid id = dataReader.GetGuid("id");
                    string name = dataReader.GetString("name");
                    Warehouse warehouse = new Warehouse(dataReader.GetString("warehouse_id"));
                    retrievedLocation = new Location(id, name, warehouse);
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedLocation;
        }
    }
}
