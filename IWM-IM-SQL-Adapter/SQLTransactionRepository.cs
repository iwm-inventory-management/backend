﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Enum;
using IWM_IM_Core.Ports;
using MySql.Data.MySqlClient;

namespace IWM_IM_SQL_Adapter
{
    public class SQLTransactionRepository : ITransactionRepository
    {
        private MySqlConnection _mySqlConnection { get; }
        private string _tableName { get; }

        public SQLTransactionRepository(MySqlConnection mySqlConnection, string tableName)
        {
            _mySqlConnection = mySqlConnection;
            _tableName = tableName;
        }

        private static readonly Dictionary<string, EMovementType> StringAssociatedWithMovementType = new Dictionary<string, EMovementType>() 
        {
            { "Input", EMovementType.Input },
            { "Output", EMovementType.Output },
            { "Adjust", EMovementType.Adjust },
            { "Inventory", EMovementType.Inventory }
        };

        private EMovementType GetMouvementTypeFromString(string movementTypeString) { return StringAssociatedWithMovementType[movementTypeString]; }

        public void Create(Transaction transaction)
        {
            string sqlRequest = @$"INSERT INTO {_tableName} (type, article_id, quantity, warehouse_id, location_id, user_id, date) 
                                                     VALUES (@type, @article_id, @quantity, @warehouse_id, @location_id, @user_id, @date)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@type", transaction.Type.ToString());
            sqlCommand.Parameters.AddWithValue("@article_id", transaction.Article.Id);
            sqlCommand.Parameters.AddWithValue("@quantity", transaction.Quantity);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", transaction.Warehouse.Id);
            sqlCommand.Parameters.AddWithValue("@location_id", transaction.Location.Id);
            sqlCommand.Parameters.AddWithValue("@user_id", transaction.User.Id);
            sqlCommand.Parameters.AddWithValue("@date", transaction.Date);

            try
            {
                _mySqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }
        }

        public List<Transaction> GetAllOnLocation(Location location, string periodStart, string periodEnd)
        {
            string minDate = string.IsNullOrWhiteSpace(periodStart) ? "0001-01-01" : periodStart;
            string maxDate = string.IsNullOrWhiteSpace(periodEnd) ? "2030-12-31" : periodEnd;

            List<Transaction> retrievedTransactions = new List<Transaction>();

            string sqlRequest = @$"SELECT * FROM {_tableName} WHERE warehouse_id = @warehouse_id 
                                                                AND location_id = @location_id
                                                                AND DATE(date) <= @period_end
                                                                AND DATE(date) >= @period_start";

            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", location.Warehouse.Id);
            sqlCommand.Parameters.AddWithValue("@location_id", location.Id);
            sqlCommand.Parameters.AddWithValue("@period_end", maxDate);
            sqlCommand.Parameters.AddWithValue("@period_start", minDate);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    Article article = new Article(dataReader.GetString("article_id"));
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    retrievedTransactions.Add(new Transaction(movementType, article, location, quantity, user, date));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedTransactions;
        }

        public List<Transaction> GetAllOnLocationForOneArticle(Location location, Article article, string periodStart, string periodEnd)
        {
            string minDate = string.IsNullOrWhiteSpace(periodStart) ? "0001-01-01" : periodStart;
            string maxDate = string.IsNullOrWhiteSpace(periodEnd) ? "2030-12-31" : periodEnd;

            List<Transaction> retrievedTransactions = new List<Transaction>();

            string sqlRequest = @$"SELECT * FROM {_tableName} WHERE warehouse_id = @warehouse_id 
                                                                AND location_id = @location_id
                                                                AND article_id = @article_id
                                                                AND DATE(date) <= @period_end
                                                                AND DATE(date) >= @period_start";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", location.Warehouse.Id);
            sqlCommand.Parameters.AddWithValue("@location_id", location.Id);
            sqlCommand.Parameters.AddWithValue("@article_id", article.Id);
            sqlCommand.Parameters.AddWithValue("@period_end", maxDate);
            sqlCommand.Parameters.AddWithValue("@period_start", minDate);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    retrievedTransactions.Add(new Transaction(movementType, article, location, quantity, user, date));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedTransactions;
        }

        public List<Transaction> GetAllOnWarehouse(Warehouse warehouse)
        {
            List<Transaction> retrievedTransactions = new List<Transaction>();

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE warehouse_id = @warehouse_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", warehouse.Id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    Article article = new Article(dataReader.GetString("article_id"));
                    Location location = new Location(dataReader.GetString("location_id"), warehouse);
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    retrievedTransactions.Add(new Transaction(movementType, article, location, quantity, user, date));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedTransactions;
        }

        public List<Transaction> GetAllOnWarehouseForOneArticle(Warehouse warehouse, Article article)
        {
            List<Transaction> retrievedTransactions = new List<Transaction>();

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE warehouse_id = @warehouse_id AND article_id = @article_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", warehouse.Id);
            sqlCommand.Parameters.AddWithValue("@article_id", article.Id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    Location location = new Location(dataReader.GetString("location_id"), warehouse);
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    retrievedTransactions.Add(new Transaction(movementType, article, location, quantity, user, date));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedTransactions;
        }

        public List<Article> GetAllArticlesOnLocation(Location location)
        {
            List<Article> retrievedArticles = new List<Article>();

            string sqlRequest = $"SELECT DISTINCT article_id FROM {_tableName} WHERE location_id = @location_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@location_id", location.Id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    retrievedArticles.Add(new Article(dataReader.GetString("article_id")));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedArticles;
        }

        public Transaction? GetLatestInventoryOnLocationForOneArticle(Location location, Article article)
        {
            Transaction latestInventory = null;

            string sqlRequest = @$"SELECT * FROM {_tableName} 
                                    WHERE location_id = @location_id
                                    AND type = 'Inventory' 
                                    AND article_id = @article_id
                                    AND date in (
                                      SELECT max(date) FROM Transactions
                                      WHERE location_id = @location_id
                                      AND type = 'Inventory'
                                      AND article_id = @article_id                                      
                                    );";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@location_id", location.Id);
            sqlCommand.Parameters.AddWithValue("@article_id", article.Id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    latestInventory = new Transaction(movementType, article, location, quantity, user, date);
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return latestInventory;
        }

        public List<Article> GetAllArticlesInWarehouse(Warehouse warehouse)
        {
            List<Article> retrievedArticles = new List<Article>();

            string sqlRequest = $"SELECT DISTINCT article_id FROM {_tableName} WHERE warehouse_id = @warehouse_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", warehouse.Id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    retrievedArticles.Add(new Article(dataReader.GetString("article_id")));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedArticles;
        }

        public Transaction? GetLatestInventoryAtDateOnLocationForOneArticle(Location location, Article article, string datetime)
        {
            Transaction latestInventory = null;

            string sqlRequest = @$"SELECT * FROM {_tableName} 
                                    WHERE location_id = @location_id
                                    AND type = 'Inventory' 
                                    AND article_id = @article_id
                                    AND date in (
                                      SELECT max(date) FROM Transactions
                                      WHERE location_id = @location_id
                                      AND date <= @datetime
                                      AND type = 'Inventory'
                                      AND article_id = @article_id                                      
                                    );";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@location_id", location.Id);
            sqlCommand.Parameters.AddWithValue("@article_id", article.Id);
            sqlCommand.Parameters.AddWithValue("@datetime", datetime);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    EMovementType movementType = GetMouvementTypeFromString(dataReader.GetString("type"));
                    int quantity = dataReader.GetInt32("quantity");
                    User user = new User(dataReader.GetString("user_id"));
                    DateTime date = dataReader.GetDateTime("date");
                    latestInventory = new Transaction(movementType, article, location, quantity, user, date);
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return latestInventory;
        }
    }
}
