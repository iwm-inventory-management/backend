﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_SQL_Adapter
{
    public class SqlConnectionStringManager
    {
        private SqlConnectionStringManager() { }

        private static SqlConnectionStringManager? _instance;

        private string _sqlConnectionString = "";

        private string _serverName = "";
        private string _serverPort = "";
        private string _databaseName = "";
        private string _user = "";
        private string _password = "";

        public static SqlConnectionStringManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new SqlConnectionStringManager();
            }
            return _instance;
        }

        public void SetConnectionStringParameters(string serverName, string serverPort, string databaseName, string user, string password)
        {
            _serverName = serverName;
            _serverPort = serverPort;
            _databaseName = databaseName;
            _user = user;
            _password = password;
        }

        public string GetConnectionString()
        {
            string connectionString = @$"server={_serverName};
                      port={_serverPort};
                      user id={_user};
                      password={_password};
                      database={_databaseName};
                      SslMode=none;
                      AllowPublicKeyRetrieval=true";
            Console.WriteLine(connectionString);
            return connectionString;
        }
    }
}
