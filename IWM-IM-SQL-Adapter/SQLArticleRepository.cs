﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using MySql.Data.MySqlClient;

namespace IWM_IM_SQL_Adapter
{
    public class SQLArticleRepository : IArticleRepository
    {
        private MySqlConnection _mySqlConnection { get; }
        private string _tableName { get; }

        public SQLArticleRepository(MySqlConnection mySqlConnection, string tableName)
        {
            _mySqlConnection = mySqlConnection;
            _tableName = tableName;
        }

        public void Create(Article article)
        {
            string sqlRequest = $"INSERT INTO {_tableName} (id) VALUES (@id)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@id", article.Id);

            try
            {
                _mySqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

        }

        public Article? GetById(string articleId)
        {
            Article? retrievedArticle = null;

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE id = @articleId";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@articleId", articleId);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    string id = dataReader.GetString("id");
                    retrievedArticle = new Article(id);
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedArticle;
        }

        public List<Article> GetAll()
        {
            List<Article> retrievedArticles = new List<Article>();

            string sqlRequest = $"SELECT * FROM {_tableName}";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    string id = dataReader.GetString("id");
                    retrievedArticles.Add(new Article(id));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedArticles;         
        }
    }
}
