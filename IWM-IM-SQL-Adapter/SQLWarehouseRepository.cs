﻿using IWM_IM_Core.Entities;
using IWM_IM_Core.Ports;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWM_IM_SQL_Adapter
{
    public class SQLWarehouseRepository : IWarehouseRepository
    {
        private MySqlConnection _mySqlConnection { get; }
        private string _tableName { get; }

        public SQLWarehouseRepository(MySqlConnection mySqlConnection, string tableName)
        {
            _mySqlConnection = mySqlConnection;
            _tableName = tableName;
        }

        public void AddLocationToWarehouse(Warehouse warehouse, Location location)
        {
            throw new NotImplementedException();
        }

        public void Create(Warehouse Warehouse)
        {
            string sqlRequest = $"INSERT INTO {_tableName} (id) VALUES (@id)";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@id", Warehouse.Id);

            try
            {
                _mySqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }

        public Warehouse? Get(string id)
        {
            Warehouse? retrievedWarehouse = null;

            string sqlRequest = $"SELECT * FROM {_tableName} WHERE id = @warehouse_id";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);
            sqlCommand.Parameters.AddWithValue("@warehouse_id", id);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    retrievedWarehouse = new Warehouse(dataReader.GetString("id"));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrievedWarehouse;
        }

        public List<Warehouse> GetAll()
        {
            List<Warehouse> retrieveWarehouses = new List<Warehouse>();

            string sqlRequest = $"SELECT * FROM {_tableName}";
            MySqlCommand sqlCommand = new MySqlCommand(sqlRequest, _mySqlConnection);

            try
            {
                _mySqlConnection.Open();
                MySqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    retrieveWarehouses.Add(new Warehouse(dataReader.GetString("id")));
                }

                _mySqlConnection.Close();
            }
            catch (MySqlException e)
            {
                _mySqlConnection.Close();
                throw e;
            }

            return retrieveWarehouses;
        }

        public void RemoveLocationFromWarehouse(Warehouse warehouse, Location location)
        {
            throw new NotImplementedException();
        }

        public void Update(Warehouse warehouse)
        {
            throw new NotImplementedException();
        }
    }
}
